﻿using System;
using System.Collections;
using System.Collections.Generic;
using Gameplay;
using UnityEngine;

public class FinishPoint : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Lamp"))
        {
            GameManager.Instance.NextFloor();

        }
    }
}
