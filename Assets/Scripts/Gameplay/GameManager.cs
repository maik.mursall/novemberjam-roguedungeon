﻿using System;
using System.Collections.Generic;
using FloorGeneration;
using TMPro;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;
using Random = UnityEngine.Random;

namespace Gameplay
{
    public class GameManager : MonoBehaviour
    {
        public static GameManager Instance;

        public int maxEnemyPerRoom;
        public int minEnemyPerRoom;

        [SerializeField] private Animator fadeOutAnimation;

        public float timeTillNextFloor = 2;

        private List<GameObject> createdRooms;
        
        private RandomFloorGenerator _randomFloorGenerator;
        private Camera _mainCamera;
        private GameObject _player;

        private static bool _gameOver = false;

        public static bool GameIsOverOrPaused => PauseManager.PauseState || _gameOver;

        public int currentFloor = 0;

        public GameObject playerPrefab;
        private PlayerStats _playerStats;
        public GameObject enemyPrefab;
        public Transform worldContainer;

        [SerializeField] private GameObject deathScreenGameObject;
        [SerializeField] private TextMeshPro deathScreenText;

        [SerializeField] private Slider slider;
        [SerializeField] private TextMeshProUGUI textMeshPro;
        
        void Start()
        {
            _gameOver = false;
            Instance = this;

            createdRooms = new List<GameObject>();

            _randomFloorGenerator = GetComponent<RandomFloorGenerator>();
            _mainCamera = Camera.main;
            
            textMeshPro.text = currentFloor.ToString();

            if (_randomFloorGenerator)
            {
                InstantiateGame();
            }
        }

        private void LateUpdate()
        {
            if (_playerStats)
            {
                slider.value = _playerStats.Health / 100f;
            }
        }

        private void ResetGame()
        {
            _randomFloorGenerator.ResetRooms();
            
            InstantiateGame();
        }
        
        private void InstantiateGame()
        {
            createdRooms = _randomFloorGenerator.GenerateAndInstantiate();

            SpawnPlayer();

            NavMeshGenerator navMeshGenerator = GetComponent<NavMeshGenerator>();

            GameObject[] floors = GameObject.FindGameObjectsWithTag("Floor");

            navMeshGenerator.SetNavMeshElements(floors);
            navMeshGenerator.BuildNavMesh();

            SpawnEnemies();

            fadeOutAnimation.SetBool("FadeIn", true);
        }

        public void NextFloor()
        {
            currentFloor++;
            
            Debug.Log(currentFloor);
            
            textMeshPro.text = currentFloor.ToString();

            StartCoroutine(WaitForFadeOut());
        }

        System.Collections.IEnumerator WaitForFadeOut()
        {
            fadeOutAnimation.SetBool("FadeIn", false);
            yield return new WaitForSeconds(1);
            ResetGame();
        }

        private void SpawnPlayer()
        {
            var spawnPosition = _randomFloorGenerator.SpawnPoint.position;
            
            spawnPosition.y = 1f;

            if (!_player)
            {
                _player = Instantiate(playerPrefab, spawnPosition, Quaternion.identity, worldContainer);
                _playerStats = _player.GetComponent<PlayerStats>();
            } else
            {
                var playerController = _player.GetComponent<PlayerMovement>().Controller;

                playerController.enabled = false;
                _player.transform.position = spawnPosition;
                playerController.enabled = true;

                FindObjectOfType<Lamp>().PickupLamp(true);
            }
            
            _mainCamera.GetComponent<CameraFollow>().target = _player.transform;
        }

        public void GameOver()
        {
            deathScreenGameObject.SetActive(true);
            _gameOver = true;
        }
        
        private void SpawnEnemies()
        {
            var rooms = _randomFloorGenerator.CreatedRooms;

            Vector3 finishPointPosition = createdRooms[0].transform.parent.GetComponentInChildren<FinishPoint>().transform.position;

            foreach (var room in createdRooms)
            {
                if (room.transform.position.Equals(new Vector3(168,0,168))) continue;
                if (room.transform.position.Equals(finishPointPosition)) continue;

                int enemyCount = Random.Range(minEnemyPerRoom, maxEnemyPerRoom);

                for(int i = 0; i < enemyCount; i++)
                {
                    Vector2 randomXZ = Random.insideUnitCircle * ((_randomFloorGenerator.RoomSize) / 2);
                    //Vector3 spawnPosition = room.transform.position + new Vector3(randomXZ.x, 0 ,randomXZ.y);

                    //TODO: Monster random spawnen lassen.

                    Vector3 spawnPosition = room.transform.position;

                    GameObject enemy = Instantiate(enemyPrefab, spawnPosition, Quaternion.identity, room.transform);

                    enemy.GetComponent<NavMeshAgent>().Move(new Vector3(randomXZ.x, 0, randomXZ.y));

                    enemy.GetComponent<NavMeshAgent>().isStopped = true;
                }              
            }
        }
    }
}
