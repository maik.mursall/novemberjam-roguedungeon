﻿using System;
using UnityEngine;
using UnityEngine.Experimental.GlobalIllumination;

namespace Gameplay
{
    public class Lamp : MonoBehaviour
    {
        [SerializeField] private Animator animator;
        private int _animatorLampId;

        [SerializeField] private KeyCode useKeyCode = KeyCode.F;
        
        [SerializeField] public Light lampLight;
        private float _lightIntensity;

        [SerializeField] private Transform lampHolder;

        [SerializeField] private float degradationRate = 5f;
        [SerializeField] private float refreshRate = 20f;

        [SerializeField] private float pickupColliderRadius = 2f;

        public float life = 100f;

        private bool _isDegrading = false;
        public bool IsGettingHold => !_isDegrading;

        private CircleRenderer _circleRenderer;

        void Start()
        {
            _lightIntensity = lampLight.intensity;
            _circleRenderer = GetComponent<CircleRenderer>();
            _animatorLampId = Animator.StringToHash("hasLantern");

        }

        private void Update()
        {
            if (Input.GetKeyUp(useKeyCode))
            {

                if (!_isDegrading)
                {
                    PlaceLamp();
                }
                else
                {
                    PickupLamp(false);
                }

                _isDegrading = !_isDegrading;
            }
        }

        void LateUpdate()
        {
            life = Mathf.Clamp(life + Time.deltaTime * (_isDegrading ? -degradationRate : refreshRate), 0f, 100f); 
            lampLight.intensity = _lightIntensity * (life / 100f);

            if (life <= 0f)
            {
                GameManager.Instance.GameOver();
            }
        }

        private void PlaceLamp()
        {
            var ownTransform = transform;

            ownTransform.parent = null;

            Vector3 newPosition = ownTransform.position;
            // newPosition.y = 0f;
                    
            ownTransform.position = newPosition;
            
            if (_circleRenderer) _circleRenderer.Activate();
            if (animator) animator.SetBool(_animatorLampId, false);
        }

        public void PickupLamp(bool force)
        {
            if (force || Physics.OverlapSphereNonAlloc(transform.position, pickupColliderRadius, new Collider[10],
                    LayerMask.GetMask("Player")) > 0)
            {
                var ownTransform = transform;

                ownTransform.parent = lampHolder;
                ownTransform.localPosition = Vector3.zero;

                if (force)
                {
                    life = 100f;
                }
                
                if (_circleRenderer) _circleRenderer.Deactivate();
                if (animator) animator.SetBool(_animatorLampId, true);

            }
        }
    }
}
