﻿using System.Collections;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class ActivateEnemiesOnEnter : MonoBehaviour
{
    private bool isTriggered = false;
    List<Animator> animators;

    private void Start()
    {
        animators = new List<Animator>();
    }

    private void Update()
    {
        if (!transform.parent.GetComponentInChildren<NavMeshAgent>())
        {
            foreach (Animator animator in animators)
            {
                if (animator.name == "Door")
                {
                    animator.SetBool("isOpen", false);
                }
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player" && !isTriggered)
        {
            isTriggered = true;

            NavMeshAgent enemyNavMashAgent = transform.parent.GetComponentInChildren<NavMeshAgent>();

            if(enemyNavMashAgent != null)
            {
                if (animators.Count == 0)
                {
                    animators.AddRange((from Animator animator in transform.parent.GetComponentsInChildren<Animator>() where animator.name.Equals("Door") select animator));
                }

                foreach(Animator animator in animators)
                {
                        animator.SetBool("isOpen", true);
                }

                NavMeshAgent[] navMeshAgents = transform.parent.GetComponentsInChildren<NavMeshAgent>();

                foreach(NavMeshAgent navMeshAgent in navMeshAgents)
                {
                    navMeshAgent.isStopped = false;
                }
            }
        }
    }
}
