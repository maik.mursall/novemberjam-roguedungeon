﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Gameplay;

public class PlayerLampBehaviour : MonoBehaviour
{
    public Light lampLight;
    public PlayerStats playerStats;
    public int anxietyValue;
    public float anxietyTempo = 0.1f;
    public float anxietyDamageTempo = 0.1f;

    [SerializeField] private AudioSource audioSource;
    private bool _isGettingAnxiety;

    // Start is called before the first frame update
    void Start()
    {
        lampLight = FindObjectOfType<Lamp>().lampLight;
        playerStats = GetComponent<PlayerStats>();
    }

    private void Update()
    {
        if(lampLight != null)
        {
            if (lampLight.range < (lampLight.transform.position - transform.position).sqrMagnitude)
            {
                if (!_isGettingAnxiety)
                {
                    audioSource.Play();
                    _isGettingAnxiety = true;
                }
                
                if(anxietyValue*anxietyTempo < 100)
                    ++anxietyValue;
                else
                {
                    playerStats.DecreaseHealth(1 * anxietyDamageTempo, false);
                }
            }
            else
            {
                if (_isGettingAnxiety)
                {
                    audioSource.Stop();
                    _isGettingAnxiety = false;
                }
                
                if(anxietyValue*anxietyTempo > 0)
                    --anxietyValue;
            }
        }
    }
}
