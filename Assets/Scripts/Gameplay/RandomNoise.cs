﻿using System.Collections;
using System.Collections.Generic;
using FloorGeneration;
using UnityEngine;

public class RandomNoise : MonoBehaviour
{
    [SerializeField] private AudioSource audioSource;
    [SerializeField] private AudioClip[] audioClips;

    [SerializeField] private float timeBetweenSounds = 10f;
    private float _timeSinceLastSound;
    private bool _active = true;

    // Update is called once per frame
    void Update()
    {
        if (_active)
        {
            if (_timeSinceLastSound >= timeBetweenSounds)
            {
                _timeSinceLastSound = 0f;
                audioSource.PlayOneShot(audioClips[Random.Range(0, audioClips.Length - 1)]);
            }
            else
            {
                _timeSinceLastSound += Time.deltaTime;
            }
        }
    }

    public void Activate()
    {
        _active = true;
    }
    
    public void Deactivate()
    {
        _active = false;
    }
}
