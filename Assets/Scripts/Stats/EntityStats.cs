﻿using System.Collections;
using System.Collections.Generic;
using FloorGeneration;
using UnityEngine;

public abstract class EntityStats : MonoBehaviour
{
    [SerializeField]
    protected float health = 0;
    public float Health => health;

    [SerializeField] protected AudioSource audioSource;
    [SerializeField] private AudioClip[] audioClips;

    [SerializeField]
    protected float speed = 0;

    public void DecreaseHealth(float damageTaken, bool playAudio)
    {
        if (playAudio && audioSource && audioClips.Length > 0)
        {
            audioSource.PlayOneShot(audioClips[Random.Range(0, audioClips.Length - 1)]);
        }
        health -= damageTaken;

        if (health <= 0)
        {
            OnDeath();
        }
    }

    public void DecreaseHealth(float damageTaken)
    {
        DecreaseHealth(damageTaken, true);
    }

    protected abstract void OnDeath();
}
