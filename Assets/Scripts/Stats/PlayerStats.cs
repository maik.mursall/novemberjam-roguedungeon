﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Gameplay;

public class PlayerStats : EntityStats
{
    public WeaponStats weaponStats;

    public void Heal(int healValue)
    {
        health += healValue;
    }

    protected override void OnDeath()
    {
        GameManager.Instance.GameOver();
    }
}
