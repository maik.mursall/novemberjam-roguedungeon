﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyStats : EntityStats
{
    public float knockbackDistance = 0;
    public int damageAmount = 0;

    public float damageCooldown = 0;

    public float timeTillAttack = 0;

    protected override void OnDeath()
    {
        if (audioSource) audioSource.Play();
        transform.GetChild(0).gameObject.SetActive(false);
        Destroy(gameObject, audioSource.clip.length);
    }
}
