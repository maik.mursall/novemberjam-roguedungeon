﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponStats : MonoBehaviour
{
    public float knockbackDistance = 0;
    public int damageAmount = 0;
    public float damageCooldown = 0;
}
