﻿using System;
using System.Collections;
using System.Collections.Generic;
using Gameplay;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseManager : MonoBehaviour
{
    [SerializeField] private KeyCode pauseKeyCode = KeyCode.Escape;
    
    public static bool PauseState = false;

    [SerializeField] private GameObject pauseTransform;

    private void Start()
    {
        PauseState = false;
    }

    public void GoBackToMenu()
    {
        Time.timeScale = 1f;
        SceneManager.LoadScene(0);
    }

    public void Restart()
    {
        Time.timeScale = 1f;
        SceneManager.LoadScene(1);
    }

    private void Pause()
    {
        Time.timeScale = 0f;
        PauseState = true;
        pauseTransform.SetActive(true);
    }
    
    public void Resume()
    {
        Debug.Log("yeet");
        Time.timeScale = 1f;
        PauseState = false;
        pauseTransform.SetActive(false);
    }

    private void Update()
    {
        if (Input.GetKeyDown(pauseKeyCode))
        {
            if (PauseState)
            {
                Resume();
            }
            else
            {
                Pause();
            }
        }
    }
}
