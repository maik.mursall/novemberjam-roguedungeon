﻿using UnityEngine;

public struct DamageSource
{
    public int damageAmount;
    public float knockbackAmount;
    public Vector3 damageDirection;

    public DamageSource(int _damageAmount, float _knockbackAmount, Vector3 _damageDirection)
    {
        damageAmount = _damageAmount;
        knockbackAmount = _knockbackAmount;
        damageDirection = _damageDirection;
    }
}
