﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FloorGeneration;

public struct RoomInfo
{
    GameObject room;
    GameObject enemy;

    public RoomInfo(GameObject _room, GameObject _enemy)
    {
        room = _room;
        enemy = _enemy;
    }
}
