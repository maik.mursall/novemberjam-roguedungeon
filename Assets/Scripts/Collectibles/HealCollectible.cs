﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealCollectible : MonoBehaviour
{
    [SerializeField] private int healValue;

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            other.GetComponent<PlayerStats>().Heal(healValue);

            Destroy(this.gameObject);
        }
    }
}
