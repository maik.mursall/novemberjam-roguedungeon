﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;

namespace FloorGeneration
{
    public class RandomFloorGenerator : MonoBehaviour
    {
        [SerializeField] private int numberOfRooms;

        [SerializeField] private Transform worldTransform;
        private Transform _roomsParent;

        [SerializeField] private float roomSize = 12f;

        public GameObject spawnPointPrefab;
        public GameObject finishPointPrefab;

        private Transform _spawnPoint;
        public Transform SpawnPoint => _spawnPoint;

        public float RoomSize => roomSize;

        private Room[,] _rooms;

        private List<Room> _createdRooms;
        public List<Room> CreatedRooms => _createdRooms;

        private Room _spawnRoom;
        public Room SpawnRoom => _spawnRoom;
        
        private Room _finishRoom;
        public Room FinishRoom => _finishRoom;

        private int _gridSize;

        private Mesh[] _wallVariations;

        void Start()
        {
            var variants = (GameObject)Resources.Load("Room/Wall/Variants/Wall_Variants", typeof(GameObject));

            Component[] meshFilter = variants.GetComponentsInChildren(typeof(MeshFilter));
            _wallVariations = meshFilter.ToList().Select(component => ((MeshFilter)component).sharedMesh).ToArray();
        }

        public List<GameObject> GenerateAndInstantiate()
        {
            GenerateDungeon();
            return InstantiateGrid();
        }

        public void ResetRooms()
        {
            foreach (Transform child in _roomsParent) {
                Destroy(child.gameObject);
            }
        }

        private Room GenerateDungeon()
        {
            _gridSize = 3 * numberOfRooms;

            _rooms = new Room[_gridSize, _gridSize];

            Vector2Int initialRoomCoordinate = new Vector2Int((_gridSize / 2) - 1, (_gridSize / 2) - 1);

            Queue<Room> roomsToCreate = new Queue<Room>();

            _spawnRoom = new Room(initialRoomCoordinate.x, initialRoomCoordinate.y);
        
            roomsToCreate.Enqueue(_spawnRoom);
            List<Room> createdRooms = new List<Room>();
            while (roomsToCreate.Count > 0 && createdRooms.Count < numberOfRooms)
            {
                Room currentRoom = roomsToCreate.Dequeue();
                this._rooms[currentRoom.RoomCoordinate.x, currentRoom.RoomCoordinate.y] = currentRoom;
                createdRooms.Add(currentRoom);
                AddNeighbors(currentRoom, roomsToCreate);
            }

            foreach (Room room in createdRooms)
            {
                List<Vector2Int> neighborCoordinates = room.NeighborCoordinates();
                foreach (Vector2Int coordinate in neighborCoordinates)
                {
                    Room neighbor = this._rooms[coordinate.x, coordinate.y];
                    if (neighbor != null)
                    {
                        room.Connect(neighbor);
                    }
                }
            }
        
            _createdRooms = createdRooms;
            
            _finishRoom = _createdRooms[Random.Range(2, _createdRooms.Count - 1)];

            return this._rooms[initialRoomCoordinate.x, initialRoomCoordinate.y];
        }

        private void AddNeighbors(Room currentRoom, Queue<Room> roomsToCreate)
        {
            List<Vector2Int> neighborCoordinates = currentRoom.NeighborCoordinates();
            List<Vector2Int> availableNeighbors = new List<Vector2Int>();

            int roomsRemaining = numberOfRooms - roomsToCreate.Count;

            if (roomsRemaining == 0) return;
        
            foreach (Vector2Int coordinate in neighborCoordinates)
            {
                if (this._rooms[coordinate.x, coordinate.y] == null)
                {
                    availableNeighbors.Add(coordinate);
                }
            }

            int randomRoomCount = Random.Range(1, Math.Min(availableNeighbors.Count, roomsRemaining));

            for (int i = 0; i < availableNeighbors.Count - randomRoomCount; i++)
            {
                availableNeighbors.RemoveAt(Random.Range(0, availableNeighbors.Count - 1));
            }

            int numberOfNeighbors = (int) Random.Range(1, availableNeighbors.Count);

            for (int neighborIndex = 0; neighborIndex < numberOfNeighbors; neighborIndex++)
            {
                float randomNumber = Random.value;
                float roomFrac = 1f / (float) availableNeighbors.Count;
                Vector2Int chosenNeighbor = new Vector2Int(0, 0);
                foreach (Vector2Int coordinate in availableNeighbors)
                {
                    if (randomNumber < roomFrac)
                    {
                        chosenNeighbor = coordinate;
                        break;
                    }
                    else
                    {
                        roomFrac += 1f / (float) availableNeighbors.Count;
                    }
                }

                roomsToCreate.Enqueue(new Room(chosenNeighbor));
                availableNeighbors.Remove(chosenNeighbor);
            }
        }


        
        private GameObject GenerateRoom(Room room)
        {
            var floor = GenerateFloor(room);

            int roomDirections = room.GetDirections();

            var doorWall = (GameObject)Resources.Load("Room/Wall/Door/Wall_Door");
            Object[] solidWalls = Resources.LoadAll("Room/Wall/Solid", typeof(GameObject));

            GameObject tempWall;
            
            // N
            if ((roomDirections & 8) > 0)
            {
                tempWall  = Instantiate(doorWall, floor.transform, false);
            }
            else
            {
                tempWall = Instantiate((GameObject) GetRandomObject(solidWalls), floor.transform, false);
            }

            tempWall.transform.name = "Wall-N";
            tempWall.transform.localPosition = new Vector3(-5f, 0f, 7f);
            tempWall.transform.localRotation = Quaternion.Euler(Vector3.up * 180f);
            
            AddRandomVariationsToWall(tempWall);
            
            // E
            if ((roomDirections & 4) > 0)
            {
                tempWall  = Instantiate(doorWall, floor.transform, false);
            }
            else
            {
                tempWall = Instantiate((GameObject) GetRandomObject(solidWalls), floor.transform, false);
            }
            tempWall.transform.name = "Wall-E";
            tempWall.transform.localPosition = new Vector3(7f, 0f, 7f);
            tempWall.transform.localRotation = Quaternion.Euler(Vector3.up * 270f);
            
            AddRandomVariationsToWall(tempWall);

            // S
            if ((roomDirections & 2) > 0)
            {
                tempWall  = Instantiate(doorWall, floor.transform, false);
            }
            else
            {
                tempWall = Instantiate((GameObject) GetRandomObject(solidWalls), floor.transform, false);
            }
            tempWall.transform.name = "Wall-S";
            tempWall.transform.localPosition = new Vector3(7f, 0f, -5f);
            tempWall.transform.localRotation = Quaternion.identity;
            
            AddRandomVariationsToWall(tempWall);

            // W
            if ((roomDirections & 1) > 0)
            {
                tempWall  = Instantiate(doorWall, floor.transform, false);
            }
            else
            {
                tempWall = Instantiate((GameObject) GetRandomObject(solidWalls), floor.transform, false);
            }
            tempWall.transform.name = "Wall-W";
            tempWall.transform.localPosition = new Vector3(-5f, 0f, -5f);
            tempWall.transform.localRotation = Quaternion.Euler(Vector3.up * 90f);

            AddRandomVariationsToWall(tempWall);

            return floor;
        }

        private void AddRandomVariationsToWall(GameObject wall)
        {
            if (_wallVariations.Length == 0) return;
            
            var wallObject = wall.transform.GetChild(0);

            for (int i = 0; i < wallObject.childCount; i++)
            {
                var wallSegment = wallObject.GetChild(i);
                if (!wallSegment.name.Contains("WallRow1")) continue;

                var randomMesh = (Mesh)_wallVariations[Random.Range(0, _wallVariations.Length - 1)];
                wallSegment.GetComponent<MeshFilter>().mesh = randomMesh;
            }
        }

        private Object GetRandomObject(Object[] objects)
        {
            return objects[Random.Range(0, objects.Length - 1)];
        }

        private GameObject GenerateFloor(Room room)
        {
            int randomIndex = Random.Range(1, 3);

            Object[] tiles = Resources.LoadAll("Room/Floor/FloorV" + randomIndex, typeof(GameObject));

            var floorObject = Instantiate(new GameObject("Floor"), new Vector3(room.RoomCoordinate.x * roomSize, 0f, room.RoomCoordinate.y * roomSize), Quaternion.identity, _roomsParent);

            for (int i = -4; i <= 6; i += 2)
            {
                for (int j = -4; j <= 6; j += 2)
                {
                    GameObject tile = Instantiate((GameObject)tiles[Random.Range(0, 4)], floorObject.transform);

                    tile.transform.localPosition = new Vector3(i, 0f, j);

                }
            }

            BoxCollider collider = floorObject.AddComponent<BoxCollider>();
            
            collider.center = new Vector3(1f, -.5f, 1f);
            collider.size = new Vector3(12f, 1f, 12f);

            GenerateDebris(floorObject);
            
            return floorObject;
        }

        private void GenerateDebris(GameObject Floor)
        {
            Object[] debrisList = Resources.LoadAll("Room/Floor/FloorObjects");
            int randomDebris = Random.Range(40, 50);
            //poition
            for(int i = 0; i < randomDebris; i++)
            {
                GameObject debris = Instantiate((GameObject)debrisList[Random.Range(0, debrisList.Length)], Floor.transform);
                debris.transform.localPosition = new Vector3(Random.Range(-5, 7), 0f, Random.Range(-5, 7));
                debris.transform.RotateAroundLocal(new Vector3(0, 1, 0), Random.Range(0f, 360f));
            }
            
        }

        private List<GameObject> InstantiateGrid()
        {
            List<GameObject> createdRooms = new List<GameObject>();

            _roomsParent = Instantiate(new GameObject("Rooms"), worldTransform).transform;
            
            foreach (var room in _createdRooms)
            {
                var builtRoom = GenerateRoom(room);

                if (room.RoomCoordinate == _finishRoom.RoomCoordinate)
                {
                    Destroy(builtRoom.transform.GetChild(14).gameObject);
                    Destroy(builtRoom.transform.GetChild(15).gameObject);
                    Destroy(builtRoom.transform.GetChild(20).gameObject);
                    Destroy(builtRoom.transform.GetChild(21).gameObject);
                    var trapDoor = Instantiate (finishPointPrefab,  builtRoom.transform, false);
                    trapDoor.transform.localPosition = new Vector3(1f, 0f, 1f);
                }
                
                createdRooms.Add(builtRoom);
            }

            _spawnPoint = Instantiate (spawnPointPrefab, new Vector3(_spawnRoom.RoomCoordinate.x, 0f, _spawnRoom.RoomCoordinate.y) * roomSize, Quaternion.identity, _roomsParent).transform;
            // Instantiate (finishPointPrefab,  _roomsParent);
            
            // 13 14 17 18
            
            return createdRooms;
        }
    }
}
