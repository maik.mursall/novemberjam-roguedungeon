using System;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

namespace FloorGeneration
{
    public class Room
    {
        public Vector2Int RoomCoordinate;
        public Dictionary<int, Room> Neighbors;

        public Room(int xCoordinate, int yCoordinate) : this(new Vector2Int(xCoordinate, yCoordinate)){}

        public Room(Vector2Int roomCoordinate)
        {
            this.RoomCoordinate = roomCoordinate;
            this.Neighbors = new Dictionary<int, Room>();
        }
        
        public List<Vector2Int> NeighborCoordinates () {
            List<Vector2Int> neighborCoordinates = new List<Vector2Int> ();
            neighborCoordinates.Add (new Vector2Int(this.RoomCoordinate.x, this.RoomCoordinate.y - 1));
            neighborCoordinates.Add (new Vector2Int(this.RoomCoordinate.x + 1, this.RoomCoordinate.y));
            neighborCoordinates.Add (new Vector2Int(this.RoomCoordinate.x, this.RoomCoordinate.y + 1));
            neighborCoordinates.Add (new Vector2Int(this.RoomCoordinate.x - 1, this.RoomCoordinate.y));
 
            return neighborCoordinates;
        }
        
        public void Connect (Room neighbor) {
            int direction = 0;
            if (neighbor.RoomCoordinate.y > this.RoomCoordinate.y) {
                direction = 8;
            }
            if (neighbor.RoomCoordinate.x > this.RoomCoordinate.x) {
                direction = 4;
            }   
            if (neighbor.RoomCoordinate.y < this.RoomCoordinate.y) {
                direction = 2;
            }
            if (neighbor.RoomCoordinate.x < this.RoomCoordinate.x) {
                direction = 1;
            }
            this.Neighbors.Add (direction, neighbor);
        }

        public int GetDirections()
        {
            int directions = 0;
            foreach (KeyValuePair<int, Room> neighborPair in Neighbors) {
                directions += neighborPair.Key;
            }

            return directions;
        }

        public string PrefabName () {
            string name = "Room_";
            int directions = GetDirections();

            name += (directions & 8) > 0 ? "N" : "";
            name += (directions & 4) > 0 ? "E" : "";
            name += (directions & 2) > 0 ? "S" : "";
            name += (directions & 1) > 0 ? "W" : "";

            return name;
        }
    }
}