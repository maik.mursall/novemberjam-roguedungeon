﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Camera))]
public class CameraFollow : MonoBehaviour
{
    public Transform target;

    public Vector3 offsetPosition;
    public Vector3 offsetRotation;
    public float smoothSpeed = 5f;

    private Vector3 _currentPosition;
    private Vector3 _targetPosition;
    
    void LateUpdate()
    {
        if (target)
        {
            var ownTransform = transform;
            _targetPosition = offsetPosition + target.position;
            ownTransform.rotation = Quaternion.Euler(offsetRotation);
        }

        _currentPosition = Vector3.Lerp(_currentPosition, _targetPosition, Time.deltaTime * smoothSpeed);
        
        transform.position = _currentPosition;
    }
}
