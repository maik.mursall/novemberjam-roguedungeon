﻿using System;
using System.Collections;
using System.Collections.Generic;
using Gameplay;
using UnityEngine;

public abstract class BaseMovement : MonoBehaviour
{
    public CharacterController Controller;
    public float speed = 2;
    // Start is called before the first frame update
    void Start()
    {
        Controller = this.GetComponent<CharacterController>();
    }

    private void Update()
    {
        Controller.enabled = !GameManager.GameIsOverOrPaused;
    }
}
