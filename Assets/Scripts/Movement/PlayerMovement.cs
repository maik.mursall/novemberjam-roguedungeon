﻿using System;
using System.Collections;
using System.Collections.Generic;
using Gameplay;
using UnityEngine;

public class PlayerMovement : BaseMovement
{
    [SerializeField] private Animator animator;
    private int _animatorSpeedId;
    
    private Camera _mainCamera;
    private Plane _hPlane;
    
    private float _moveHor;
    private float _moveVert;

    [SerializeField] private RandomNoise randomNoise;

    private void Update()
    {
        if (GameManager.GameIsOverOrPaused) return;
        
        _moveHor = Input.GetAxis("Horizontal");
        _moveVert = Input.GetAxis("Vertical");
        

        Controller.SimpleMove(Vector3.ClampMagnitude(new Vector3(_moveHor, 0, _moveVert), 1f) * speed);

        LookAtMouse();

        // apply the _impact force:
        if (_impact.magnitude > 0.2F) _character.Move(_impact * Time.deltaTime);
        // consumes the _impact energy each cycle:
        _impact = Vector3.Lerp(_impact, Vector3.zero, 5 * Time.deltaTime);
    }

    private void LateUpdate()
    {
        if (animator)
        {
            animator.SetFloat(_animatorSpeedId, _moveHor + _moveVert);

            if (!GameManager.GameIsOverOrPaused && (Math.Abs(_moveHor) + Math.Abs(_moveVert))> 0)
            {
                randomNoise.Activate();
            }
            else
            {
                randomNoise.Deactivate();
            }
        }
    }

    private Vector3 _impact = Vector3.zero;
    private CharacterController _character;
    // Use this for initialization
    void Start()
    {
        _character = GetComponent<CharacterController>();
        _mainCamera = Camera.main;

        _hPlane = new Plane(Vector3.up, Vector3.zero);

        _animatorSpeedId = Animator.StringToHash("speed");
        randomNoise.Deactivate();
    }

    // call this function to add an _impact force:
    public void AddImpact(Vector3 dir, float force)
    {
        dir.Normalize();
        if (dir.y < 0) dir.y = -dir.y; // reflect down force on the ground
        _impact += dir.normalized * (force / GetComponent<Rigidbody>().mass);
    }

    private void LookAtMouse()
    {
        Ray ray = _mainCamera.ScreenPointToRay(Input.mousePosition);
        
        if (_hPlane.Raycast(ray, out float distance)){
            Vector3 hitPoint = ray.GetPoint(distance);

            Vector3 playerToMouse = hitPoint - transform.position;

            Vector3 lookRot = Quaternion.LookRotation(playerToMouse, Vector3.up).eulerAngles;

            lookRot.x = 0f;
            lookRot.z = 0f;

            transform.rotation = Quaternion.Euler(lookRot);
        }
    }
}
