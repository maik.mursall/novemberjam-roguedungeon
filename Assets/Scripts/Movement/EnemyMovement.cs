﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyMovement : BaseMovement
{
    public NavMeshAgent agent;

    private Vector3 _impact = Vector3.zero;
    private CharacterController _character;

    private void Start()
    {
        agent = GetComponent<NavMeshAgent>();
    }

    // Update is called once per frame
    void Update()
    {
        agent.SetDestination(GameObject.FindGameObjectsWithTag("Lamp")[0].gameObject.transform.position);


        // apply the _impact force:
        if (_impact.magnitude > 0.2F) GetComponent<NavMeshAgent>().Move(_impact * Time.deltaTime);
        // consumes the _impact energy each cycle:
        _impact = Vector3.Lerp(_impact, Vector3.zero, 5 * Time.deltaTime);
    }

    public void AddImpact(Vector3 dir, float force)
    {
        dir.Normalize();
        if (dir.y < 0) dir.y = -dir.y; // reflect down force on the ground
        _impact += dir.normalized * (force / GetComponent<Rigidbody>().mass);
    }
}
