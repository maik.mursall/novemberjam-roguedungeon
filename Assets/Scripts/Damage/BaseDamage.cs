﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class BaseDamage : MonoBehaviour
{
    protected float damageCooldown;

    protected bool damageOnCooldown = false;

    protected float timeSinceDamageCooldown = 0;
}
