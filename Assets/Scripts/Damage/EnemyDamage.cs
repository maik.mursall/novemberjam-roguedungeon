﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using Gameplay;

public class EnemyDamage : BaseDamage
{
    [SerializeField] private Animator animator;
    private int _animatorAttackId;
    
    public EnemyStats enemyStats;
    private NavMeshAgent navMeshAgent;
    private float timeTillAttack;
    private bool playerInAttackRange = false;
    private bool lanternInAttackRange = false;

    private void Awake()
    {
        damageCooldown = enemyStats.damageCooldown;
        timeTillAttack = enemyStats.timeTillAttack;
        navMeshAgent = GetComponent<NavMeshAgent>();
        _animatorAttackId = Animator.StringToHash("attack");
    }

    private void LateUpdate()
    {
        if (damageOnCooldown)
        {
            timeSinceDamageCooldown += Time.deltaTime;
            
            if (timeSinceDamageCooldown >= damageCooldown)
            {
                timeSinceDamageCooldown = 0f;
                damageOnCooldown = false;
                navMeshAgent.isStopped = false;
            }
        }
    }

    private void OnTriggerStay(Collider collider)
    {
        if (collider.tag == "Player" && !damageOnCooldown && navMeshAgent.isActiveAndEnabled)
        {
            navMeshAgent.isStopped = true;
            playerInAttackRange = true;
            damageOnCooldown = true;

            PlayerStats playerStats = collider.GetComponent<PlayerStats>();
            PlayerMovement playerMovement = collider.GetComponent<PlayerMovement>();
            Vector3 direction = collider.transform.position - transform.position;
            direction.y = 0;

            StartCoroutine(WaitForDamage(playerStats, playerMovement, collider));
        }
        else if(collider.tag == "Player" && navMeshAgent.isActiveAndEnabled)
        {
            navMeshAgent.isStopped = true;
            playerInAttackRange = true;
        }



        if (collider.tag == "Lamp" && !damageOnCooldown && navMeshAgent.isActiveAndEnabled)
        {
            navMeshAgent.isStopped = true;
            lanternInAttackRange = true;
            damageOnCooldown = true;

            Lamp lamp = collider.GetComponent<Lamp>();

            StartCoroutine(WaitForDamageOnLantern(lamp));
        }
        else if(collider.tag == "Lamp" && navMeshAgent.isActiveAndEnabled)
        {
            navMeshAgent.isStopped = true;
            lanternInAttackRange = true;
        }
    }

    IEnumerator WaitForDamage(PlayerStats playerStats, PlayerMovement playerMovement, Collider collider)
    {
        animator.SetTrigger(_animatorAttackId);
        yield return new WaitForSeconds(timeTillAttack);

        navMeshAgent.isStopped = false;

        if (playerInAttackRange)
        {
            Vector3 direction = collider.transform.position - transform.position;
            direction.y = 0;
            DoDamage(playerStats, playerMovement, new DamageSource(enemyStats.damageAmount, enemyStats.knockbackDistance, direction));
        }
    }

    IEnumerator WaitForDamageOnLantern(Lamp lamp)
    {
        animator.SetTrigger(_animatorAttackId);
        yield return new WaitForSeconds(timeTillAttack);

        navMeshAgent.isStopped = false;

        if (lanternInAttackRange)
        {
            lamp.life -= enemyStats.damageAmount;
        }
    }

    private void OnTriggerExit(Collider collider)
    {
        playerInAttackRange = false;
        lanternInAttackRange = false;
    }

    public void DoDamage(EntityStats damageTarget, PlayerMovement playerMovement, DamageSource damageSource)
    {
        damageTarget.DecreaseHealth(damageSource.damageAmount);
        playerMovement.AddImpact(damageSource.damageDirection, damageSource.knockbackAmount);
    }
}
