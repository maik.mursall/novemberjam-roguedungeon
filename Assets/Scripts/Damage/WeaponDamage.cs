﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Gameplay;

public class WeaponDamage : BaseDamage
{
    [SerializeField] private Animator animator;
    private int _animatorAttackId;
    
    public WeaponStats weaponStats;
    public Collider weaponCollider;
    public Lamp lamp;

    [SerializeField] private AudioSource audioSource;
    [SerializeField] private AudioClip hitSFX;

    private void Start()
    {
        damageCooldown = weaponStats.damageCooldown;
        weaponCollider.isTrigger = false;
        lamp = FindObjectOfType<Lamp>();
        
        _animatorAttackId = Animator.StringToHash("attack");
    }

    private void Update()
    {
        if (damageOnCooldown)
        {
            timeSinceDamageCooldown += Time.deltaTime;

            if (timeSinceDamageCooldown >= damageCooldown)
            {
                damageOnCooldown = false;
                timeSinceDamageCooldown = 0;
            }
        }
        else
        {
            if (!lamp.IsGettingHold && Input.GetMouseButtonDown(0))
            {
                weaponCollider.isTrigger = true;
                
                if (animator) animator.SetTrigger(_animatorAttackId);
            }
        }
    }

    private void OnTriggerEnter(Collider collider)
    {
        Debug.Log("col" + gameObject.name);
        if (collider.gameObject.tag == "Enemy" && !damageOnCooldown && !collider.isTrigger && !lamp.IsGettingHold)
        {
            damageOnCooldown = true;

            EnemyStats enemyStats = collider.gameObject.GetComponent<EnemyStats>();
            EnemyMovement enemyMovement = collider.gameObject.GetComponent<EnemyMovement>();

            Vector3 direction = collider.transform.position - transform.position;
            direction.y = 0;

            DoDamage(enemyStats, enemyMovement, new DamageSource(weaponStats.damageAmount, weaponStats.knockbackDistance, transform.forward));

            weaponCollider.isTrigger = false;
        }
    }

    public void DoDamage(EntityStats damageTarget, EnemyMovement enemyMovement, DamageSource damageSource)
    {
        damageTarget.DecreaseHealth(damageSource.damageAmount);
        enemyMovement.AddImpact(damageSource.damageDirection, damageSource.knockbackAmount);
        audioSource.PlayOneShot(hitSFX);
    }
}
